package com.delta.blog.blog.services;

import java.util.List;

import com.delta.blog.blog.models.Post;

public interface PostService {

    List<Post> findAll();

    // method to save an object of our model - "Post"
    Post save(Post post);

    // method to find a object by the id
    Post findById(Long id);

    // method to delete a object from our database
    void delete(Long id);
    
}
