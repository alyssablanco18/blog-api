package com.delta.blog.blog.services;

import java.util.List;

import com.delta.blog.blog.models.Post;
import com.delta.blog.blog.repositories.PostRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    // bring in our repo
    @Autowired
    PostRepo postRepo;

    @Override
    public List<Post> findAll() {
        
        return postRepo.findAll();
    }


    @Override
    public Post save(Post post) {
        postRepo.save(post);
        return post;
    }


    @Override
    public Post findById(Long id) {
        if(postRepo.findById(id).isPresent()) {
            return postRepo.findById(id).get();
        }

        return null;
    }


    @Override
    public void delete(Long id) {
        Post postToDelete = findById(id)
        postRepo.delete(postToDelete);
    }

    
}
